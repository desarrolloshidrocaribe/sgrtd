#Sistema de Gestion de RTD
Sistema para la gestion de la recepcion y tramitacion de documentos en la C.A. Hidrologica del Caribe

## Requerimientos
### Servidor
* PHP >= 5.4
* Extension MCrypt de PHP
* Motor de Base de datos PostgreSQL o MySQL
* Extensiones PDO de PHP para el motor de base de datos que vaya a utilizar
#### Envio de Email:
* Beanstalkd (Cola de mensajes de correo electronico)
* Supervisord (Administrador del servicio Beanstalkd)
#### Envio de SMS
* Datos de acceso al webservice playSMS

### Cliente
Navegador web compatible con HTML5 (Google Chrome, Firefox)

### Instalacion
Este sistema utiliza [Composer](http://getcomposer.org) para la instalacion y manejo de dependencias. Si no lo tienes actualmente, comience por [instalar Composer](http://getcomposer.org/doc/00-intro.md).

Situese en la raiz del proyecto desde la consola y ejecute:

    composer update

Una vez que composer ha finalizado con la instalacion y actualizacion de dependencias, edite al archivo `app/config/database.php` y configurelo con los valores apropiados segun su base de datos.

Luego, ejecute la instalacion de las tablas de control de migraciones escribiendo en la consola:
	
	php artisan migrate:install

Despues de esto, proceda a realizar la migracion escribiendo en la consola el siguiente comando:

	php artisan migrate --seed

Al finalizar la migracion podra acceder al sistema utilizando el usuario y contraseña por defecto.

#### Envio de Email
Los correos electronicos generados por el sistema son enviados a una cola para que el desempeño de la aplicacion no se vea afectado drasticamente.

Para activar la cola se debe ejecutar el siguiente comando.

	php artisan queue:listen --timeout=0

De este modo pondra en funcionamiento la cola de mensajes de correo electronico gestionada por Beanstalkd, este comando debe mantenerse en ejecucion o sus correos electronicos no podran ser enviados. Para la automatizar su ejecucion haga uso de la herramienta Supervisord.

#### Envio de SMS
Edite el archivo `app/config/packages/javierslzrh/playsmsws/config.php` y configurelo con los valores de playSMS apropiados.

## Desarrollador
Javier Salazar <javieslzrh@gmail.com>
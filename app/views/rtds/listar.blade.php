@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Gesti&oacute;n de RTDs</legend>
        <table class="table">         
        <thead>
            <tr>
                <th>Documento</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rtds as $rtd)
            <tr>
                <td><a href="rtds/{{ $rtd->id }}">{{ $rtd->tipodocumento->descripcion }} N° {{ $rtd->numerodocumento }} </a> </td>
                <td>
                    <div class="action-buttons">
                        {{ Form::open(array('method'=>'DELETE','route'=>array('rtds.destroy',$rtd->id))) }}
                            <a href="receptoresrtds/{{ $rtd->id }}/edit" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
                            <button type="submit" class="btn-glyphicon trash" title="Eliminar" onclick="return confirm('Esta seguro que desea eliminar el RTD?') ? true : false"    >
                                <span class="glyphicon glyphicon-trash">
                            </button>
                        {{ Form::close() }}
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </div>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="paging">
                    <ul class="pagination pagination-sm"></ul>
                </div>
        </td>
    </tr>
</tfoot>
</table>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('table').footable();
});
</script>
@stop
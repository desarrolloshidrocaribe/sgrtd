@extends('master')
@section('content')
<div class="row">
	{{ Form::open(array('route' => 'rtds.store')) }}
	<legend>Recepci&oacute;n y tramitaci&oacute;n de documentos</legend>

	<div class="col-md-4">
		{{ Form::label('fecha', 'Fecha'); }}
		{{ Form::text('fecha', $fecha, [ 'class'=>'form-control', 'readonly'=>'readonly' ] ); }}
	</div>

	<div class="col-md-4">
		{{ Form::label('tipodocumento', 'Tipo de documento'); }}
		{{ Form::select('tipodocumento', array('' => 'Seleccione')+TipoDocumento::all()->lists('descripcion', 'id'), Input::old('tipodocumento'), ['class' => 'form-control']) }}
	</div>

	<div class="col-md-4">
		{{ Form::label('otrotipodocumento', 'Otro tipo de documento'); }}
		{{ Form::text('otrotipodocumento', Input::old('otrotipodocumento'), ['class' => 'form-control']); }}
	</div>


	<div class="col-md-4">
		{{ Form::label('numerodocumento','N° de documento'); }} 
		{{ Form::text('numerodocumento', Input::old('numerodocumento'), ['class' => 'form-control']); }}
	</div>

	<div class="col-md-4">
		{{ Form::label('fechatramitacion', 'Fecha de Tramitación'); }}
		<div class='input-group date' id='datetimepicker2'>
			{{ Form::text('fechatramitacion', Input::old('fechatramitacion'), ['class' => 'form-control']); }}
			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		</div>
	</div>

	<div class="col-md-4">
		{{ Form::label('fecharecepcion', 'Fecha de Recepción'); }}
		<div class='input-group date' id='datetimepicker3'>
			{{ Form::text('fecharecepcion', Input::old('fecharecepcion'), ['class' => 'form-control']); }}
			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		</div>
	</div>

	<div class="col-md-7">
		{{ Form::label('receptoresrtd', 'Remisión de Presidencia para'); }}
		<select id="receptoresrtd" name="receptoresrtd[]" class="form-control" multiple="multiple">
			@foreach ($grupos as $grupo)
				<optgroup value="{{ $grupo->id }}" label="{{ $grupo->nombre }}">
					@foreach ($grupo->receptoresRTD as $receptorrtd)   
						<option value="{{ $receptorrtd->id }}"
                            @if(Input::old('receptoresrtd'))
                                @foreach(Input::old('receptoresrtd') as $idReceptor)
                                    @if($receptorrtd->id == $idReceptor) 
                                        {{ 'selected = "selected"' }} 
                                    @endif
                                @endforeach
                            @endif
							>{{ $receptorrtd->nombre }}</option>
					@endforeach
				</optgroup>
			@endforeach
	</select>

</div>

<div class="col-md-5">
	{{ Form::label('otroreceptorrtd', 'Remitir a otro'); }}
	{{ Form::text('otroreceptorrtd',Input::old('otroreceptorrtd'), ['class' => 'form-control'] ); }}
</div>

<div class="col-md-12">
	{{ Form::label('instrucciones', 'Instrucciones'); }}
	{{ Form::select('instrucciones', Instruccion::lists('descripcion', 'id'), Input::old('instrucciones'), ['class' => 'form-control', 'multiple' => 'multiple', 'name' => 'instrucciones[]']); }}

</div>

<div class="col-md-7">
	{{ Form::label('puntocuentainformacion', 'Punto de Cuenta/Información'); }}
	{{ Form::select('puntocuentainformacion', ReceptorPunto::lists('nombre', 'id'), Input::old('puntocuentainformacion'), ['class' => 'form-control', 'multiple' => 'multiple', 'name' => 'puntocuentainformacion[]']); }}
</div>

<div class="col-md-5">
	{{ Form::label('otropuntocuentainformacion', 'Otro Pto. Cuenta/Información'); }}
	{{ Form::text('otropuntocuentainformacion', Input::old('otropuntocuentainformacion'), ['class' => 'form-control'] ); }}
</div>

<div class="col-md-12">
	{{ Form::label('coordinarcon', 'Coordinar con'); }}
	{{ Form::text('coordinarcon', Input::old('coordinarcon'), ['class' => 'form-control'] ); }}
</div>

<div class="col-md-12">
	{{ Form::label('observaciones', 'Observaciones'); }}
	{{ Form::textarea('observaciones', Input::old('observaciones'), ['class' => 'form-control', 'rows' => 3] ); }}
</div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('rtds') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Crear', ['class' => 'btn btn-success btn-fixed ']); }}
        </div>
    </div>
</div>
{{ Form::close() }}
@stop
@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del RTD</legend>
                <table class="table">
                    <tbody>
                        <tr><td colspan="4"><h4>Recepci&oacute;n y Tramitaci&oacute;n de Documentos (RTD)</h4></td><tr>
                        <tr>
                            <th>Tipo de documento</th>
                            <th>Fecha de tramitaci&oacute;n</th>
                            <th>Fecha de recepci&oacute;n</th>
                        </tr>
                        <tr>
                            <td>@if($rtd->tipodocumento) {{ $rtd->tipodocumento->descripcion }} @else {{ $rtd->otrotipodocumento }} @endif</td>
                            <td>{{ $rtd->fechatramitacion }}</td>
                            <td>{{ $rtd->fecharecepcion }}</td>
                        </tr>
                        <tr>
                            <th>Remision de presidencia para</th>
                        </tr>
                        <tr>
                            <td>
                            @foreach($rtd->receptoresrtd as $receptorrtd)
                            <p> {{ $receptorrtd->nombre }} </p>
                            @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table">
                    <caption style="font-weight:bold;">Comentarios:</caption>
                    <tbody>
                        <tr>
                            <td>
                            @foreach($rtd->comentarios as $comentario)
                            <a><span class="glyphicon glyphicon-user"></span> {{ $comentario->autor->nombres." ".$comentario->autor->apellidos }} </a>
                            <p class="well"><span class="glyphicon glyphicon-comment "></span> {{ $comentario->comentario }} </p>
                            @endforeach
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
        <a href="{{ URL::to('rtds') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
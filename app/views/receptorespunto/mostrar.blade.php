@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del Receptor de Punto de Cuenta</legend>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Nombre:</th>
                            <td>{{ $receptorpunto['nombre'] }}</td>
                        </tr>
                    </tbody>
                </table>
        <a href="{{ URL::to('receptorespunto') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
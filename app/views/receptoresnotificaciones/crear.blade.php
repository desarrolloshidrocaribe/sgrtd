@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('route' => 'notificaciones.store')) }}
    <legend>Nuevo Receptor de Notificaciones RTD</legend>
    <div class="col-md-6">
        {{ Form::label('nombres', 'Nombres'); }}
        {{ Form::text('nombres', Input::old('nombres'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('apellidos', 'Apellidos'); }}
        {{ Form::text('apellidos', Input::old('apellidos'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('email', 'Correo Electrónico'); }}
        {{ Form::email('email', Input::old('email'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('telefonomovil', 'Teléfono Móvil'); }}
        {{ Form::text('telefonomovil', Input::old('telefonomovil'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-12">
        {{ Form::label('receptoresrtd', 'Notificar al remitir a:'); }}
        <select id="receptoresrtd" name="receptoresrtd[]" class="form-control" multiple>
            @foreach ($grupos as $grupo)
                <optgroup value="{{ $grupo->id }}" label="{{ $grupo->nombre }}">
                    @foreach ($grupo->receptoresRTD as $receptorrtd)   
                        <option value="{{ $receptorrtd->id }}"
                            @if(Input::old('receptoresrtd'))
                                @foreach(Input::old('receptoresrtd') as $idReceptor)
                                    @if($receptorrtd->id == $idReceptor) 
                                        {{ 'selected = "selected"' }} 
                                    @endif
                                @endforeach
                            @endif
                            >{{ $receptorrtd->nombre }}</option>
                    @endforeach                
                </optgroup>
            @endforeach
        </select>
    </div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('notificaciones') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Crear', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('method' => 'put', 'route' => array('notificaciones.update', $receptornotificacionRTD->id))) }}
    <legend>Editar Receptor de Notificaciones RTD</legend>
    {{ Form::hidden('id', $receptornotificacionRTD->id) }}
    <div class="col-md-6">
        {{ Form::label('nombres', 'Nombres'); }}
        {{ Form::text('nombres', Input::old('nombres') ? Input::old('nombres') : $receptornotificacionRTD->nombres, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('apellidos', 'Apellidos'); }}
        {{ Form::text('apellidos', Input::old('apellidos') ? Input::old('apellidos') : $receptornotificacionRTD->apellidos, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('email', 'Correo Electrónico'); }}
        {{ Form::email('email', Input::old('email') ? Input::old('email') : $receptornotificacionRTD->email, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('telefonomovil', 'Teléfono Móvil'); }}
        {{ Form::text('telefonomovil', Input::old('telefonomovil') ? Input::old('telefonomovil') : $receptornotificacionRTD->telefonomovil, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-12">
        {{ Form::label('receptoresrtd', 'Receptores RTD'); }}
        <select id="receptoresrtd" name="receptoresrtd[]" class="form-control" multiple>
            @foreach ($grupos as $grupo)
                <optgroup value="{{ $grupo->id }}" label="{{ $grupo->nombre }}">
                    @foreach ($grupo->receptoresRTD as $receptorrtd)   
                        <option value="{{ $receptorrtd->id }}" 
                            @foreach($receptornotificacionRTD->receptoresrtd as $receptor)
                                @if($receptorrtd->id == $receptor->id) 
                                    {{ 'selected = "selected"' }} 
                                @endif
                            @endforeach
                            >{{ $receptorrtd->nombre }}</option>
                    @endforeach                
                </optgroup>
            @endforeach
        </select>
    </div>
    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('notificaciones') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del Receptor de Notificaciones</legend>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Nombres:</td>
                            <td>{{ $receptornotificacionRTD->nombres }}</td>
                        </tr>
                        <tr>
                            <th>Apellidos:</td>
                            <td>{{ $receptornotificacionRTD->apellidos }}</td>
                        </tr>
                        <tr>
                            <th>Correo Electr&oacute;nico</td>
                            <td>{{ $receptornotificacionRTD->email }}</td>
                        </tr>
                        <tr>
                            <th>Teléfono M&oacute;vil</td>
                            <td>{{ $receptornotificacionRTD->telefonomovil }}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table">
                    <caption style="font-weight:bold;">Notificar al remitir a:</caption>
                    <tbody>
                            @foreach($receptornotificacionRTD->receptoresrtd as $receptorrtd)
                            <tr><td>{{ $receptorrtd->nombre }}</td></tr>
                            @endforeach
                        
                    </tbody>
                </table>
        <a href="{{ URL::to('notificaciones') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
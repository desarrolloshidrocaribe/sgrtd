@extends('base')
@section('content')
{{ HTML::style('css/core/main.css') }}

<div class="panel">
	<div class="col-sm-6 col-md-3 col-md-offset-4">
		{{ Form::open(array('route' => 'sesion.store', 'class' => 'form-signin')) }}
		<div class="login-wall">
			<img class="profile-img" src="imagenes/logo-hidro.png" alt="">
			<h2 class="titulo text-primary">SGRTD</h2>
			<div style="margin-bottom: 25px" class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
				{{ Form::text('usuario', Input::old('Usuario'),['placeholder'=>'Usuario', 'class'=>'form-control']); }}
			</div>
			<div style="margin-bottom: 25px" class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				{{ Form::password('password', ['placeholder' => 'Contraseña', 'class' => 'form-control']); }}
			</div>
			<div class="input-group">
				<div class="checkbox">
					<label>
						{{ Form::checkbox('recordarme', '1'); }} No cerrar sesi&oacute;n
					</label>
				</div>
			</div>
			{{ Form::submit('Iniciar Sesión', ['class' => 'btn btn-lg btn-primary btn-block']) }}
		</div>
	</div>
</div>
{{ Form::close() }}
@stop

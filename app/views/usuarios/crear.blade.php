@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('route' => 'usuarios.store')) }}
    <legend>Nuevo usuario</legend>
    <div class="col-md-6">
        {{ Form::label('nombres', 'Nombres'); }}
        {{ Form::text('nombres', Input::old('nombres'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('apellidos', 'Apellidos'); }}
        {{ Form::text('apellidos', Input::old('apellidos'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('usuario', 'Nombre de usuario'); }}
        {{ Form::text('usuario', Input::old('usuario'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('nivelacceso', 'Nivel de acceso'); }}
        {{ Form::select('nivelacceso', array('' => 'Seleccione')+array(Usuario::USUARIO => 'Usuario', Usuario::ADMINISTRADOR => 'Administrador'), Input::old('nivelacceso'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('password', 'Contraseña'); }}
        {{ Form::password('password', ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('password_confirmation', 'Confirmar contraseña'); }}
        {{ Form::password('password_confirmation', ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('email', 'Correo Electrónico'); }}
        {{ Form::email('email', Input::old('email'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('telefonomovil', 'Teléfono Móvil'); }}
        {{ Form::text('telefonomovil', Input::old('telefonomovil'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('usuarios') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Crear', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
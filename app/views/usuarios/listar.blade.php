@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Gesti&oacute;n de Usuarios</legend>
        <a class='btn btn-primary btn-fixed' href="usuarios/create"><span class="glyphicon glyphicon-plus-sign"></span> Nuevo</a></p>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombres y Apellidos</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($usuarios as $usuario)
                    <tr>
                        <td><a href="usuarios/{{ $usuario->id }}">{{ $usuario->nombres." ".$usuario->apellidos }}</a></td>
                        <td>
                            <div class="action-buttons">
                                {{ Form::open(array('method'=>'DELETE','route'=>array('usuarios.destroy',$usuario->id))) }}
                                    <a href="usuarios/{{ $usuario->id }}/edit" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <button type="submit" class="btn-glyphicon trash" title="Eliminar" onclick="return confirm('Esta seguro que desea eliminar al usuario?') ? true : false"    >
                                        <span class="glyphicon glyphicon-trash">
                                    </button>
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">
                        <div class="paging">
                            <ul class="pagination pagination-sm"></ul>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('table').footable();
});
</script>
@stop
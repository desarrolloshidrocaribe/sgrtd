@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del usuario</legend>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Nombres:</th>
                            <td>{{ $usuario->nombres }}</td>
                        </tr>
                        <tr>
                            <th>Apellidos:</th>
                            <td>{{ $usuario->apellidos }}</td>
                        </tr>
                        <tr>
                            <th>Usuario:</th>
                            <td>{{ $usuario->usuario }}</td>
                        </tr>
                        <tr>
                            <th>Correo Electrónico</th>
                            <td>{{ $usuario->email }}</td>
                        </tr>
                        <tr>
                            <th>Teléfono Móvil</th>
                            <td>{{ $usuario->telefonomovil }}</td>
                        </tr>
                        <tr>
                            <th>Nivel de Acceso</th>
                            <td>{{ $usuario->nivelacceso }}</td>
                        </tr>
                    </tbody>
                </table>
        <a href="{{ URL::to('usuarios') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
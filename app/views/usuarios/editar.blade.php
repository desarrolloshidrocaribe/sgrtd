@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('method' => 'put', 'route' => array('usuarios.update', $usuario->id))) }}
    <legend>Actualizar datos del usuario</legend>
    {{ Form::hidden('id', $usuario->id) }}
    <div class="col-md-6">
        {{ Form::label('nombres', 'Nombres'); }}
        {{ Form::text('nombres', Input::old('nombres') ? Input::old('nombres') : $usuario->nombres, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('apellidos', 'Apellidos'); }}
        {{ Form::text('apellidos', Input::old('apellidos') ? Input::old('apellidos') : $usuario->apellidos, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('usuario', 'Nombre de usuario'); }}
        {{ Form::text('usuario', Input::old('usuario') ? Input::old('usuario') : $usuario->usuario, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('nivelacceso', 'Nivel de acceso'); }}
        {{ Form::select('nivelacceso', array('' => 'Seleccione')+array(Usuario::USUARIO => 'Usuario', Usuario::ADMINISTRADOR => 'Administrador'), Input::old('nivelacceso') ? Input::old('nivelacceso') : $usuario->nivelacceso, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('password', 'Contraseña'); }}
        {{ Form::password('password', ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('password_confirmation', 'Confirmar contraseña'); }}
        {{ Form::password('password_confirmation', ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('email', 'Correo Electrónico'); }}
        {{ Form::text('email', Input::old('email') ? Input::old('email') : $usuario->email, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('telefonomovil', 'Teléfono Móvil'); }}
        {{ Form::text('telefonomovil', Input::old('telefonomovil') ? Input::old('telefonomovil') : $usuario->telefonomovil, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('usuarios') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
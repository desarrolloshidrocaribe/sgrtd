@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del Receptor de RTD</legend>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Descripci&oacute;n:</th>
                            <td>{{ $instruccion->descripcion }}</td>
                        </tr>
                    </tbody>
                </table>
        <a href="{{ URL::to('instrucciones') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('method' => 'put', 'route' => array('instrucciones.update', $instruccion->id))) }}
    <legend>Editar instrucci&oacute;n</legend>
    {{ Form::hidden('id', $instruccion->id) }}
    <div class="col-md-12">
        {{ Form::label('descripcion', 'Descripción'); }}
        {{ Form::text('descripcion', Input::old('descripcion') ? Input::old('descripcion') : $instruccion->descripcion, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('instrucciones') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
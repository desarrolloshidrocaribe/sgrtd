@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('method' => 'put', 'route' => array('receptoresrtds.update', $receptorrtd->id))) }}
    {{ Form::hidden('id', $receptorrtd->id) }}
    <legend>Actualizar Receptor de RTD</legend>
    <div class="col-md-12">
        {{ Form::label('nombre', 'Nombre'); }}
        {{ Form::text('nombre', Input::old('nombre') ? Input::old('nombre') : $receptorrtd->nombre, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('responsable', 'Usuario responsable'); }}
        {{ Form::select('responsable', array('' => 'Seleccione')+Usuario::all()->lists('usuario', 'id'), Input::old('usuario') ? Input::old('usuario') : $receptorrtd->id_usuario, ['class' => 'form-control']) }}
    </div>

    <div class="col-md-6">
        {{ Form::label('grupo', 'Grupo'); }}
        {{ Form::select('grupo', array('' => 'Seleccione')+GrupoReceptoresRTD::all()->lists('nombre', 'id')+array('-1' => 'Ninguno'), Input::old('grupo') ? Input::old('grupo') : $receptorrtd->id_grupo, ['class' => 'form-control']) }}
        
    </div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('receptoresrtds') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
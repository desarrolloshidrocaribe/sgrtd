@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('route' => 'receptoresrtds.store')) }}
    <legend>Nuevo Receptor de RTD</legend>
    <div class="col-md-12">
        {{ Form::label('nombre', 'Nombre'); }}
        {{ Form::text('nombre', Input::old('nombre'), ['class' => 'form-control']); }}
    </div>

    <div class="col-md-6">
        {{ Form::label('responsable', 'Usuario responsable'); }}
        {{ Form::select('responsable', array('' => 'Seleccione')+Usuario::all()->lists('usuario', 'id'), Input::old('usuario'), ['class' => 'form-control']) }}
    </div>

    <div class="col-md-6">
        {{ Form::label('grupo', 'Grupo'); }}
        {{ Form::select('grupo', array('' => 'Seleccione')+GrupoReceptoresRTD::all()->lists('nombre', 'id')+array('-1' => 'Ninguno'), Input::old('grupo'), ['class' => 'form-control']) }}
        
    </div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('receptoresrtds') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Crear', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del Receptor de RTD</legend>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Nombre:</th>
                            <td>{{ $receptorrtd->nombre }}</td>
                        </tr>
                        <tr>
                            <th>Grupo:</th>
                            <td>@if($receptorrtd->grupo) {{ $receptorrtd->grupo->nombre }} @else {{ "Ninguno" }} @endif</td>
                        </tr>
                        <tr>
                            <th>Responsable:</th>
                            <td>{{ $receptorrtd->responsable->nombres. " ".$receptorrtd->responsable->apellidos." (".$receptorrtd->responsable->usuario.")" }}</td>
                        </tr>
                    </tbody>
                </table>
        <a href="{{ URL::to('receptoresrtds') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
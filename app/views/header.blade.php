<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
	        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>.::SGRTD::.</title>
	    	{{ HTML::style('css/normalize/normalize.min.css') }} 
			{{ HTML::style('css/jquery/plugins/bootstrap/bootstrap.min.css') }}
            {{ HTML::style('css/jquery/plugins/select2/select2.css') }}
            {{ HTML::style('css/jquery/plugins/select2/select2-bootstrap.css') }}
            {{ HTML::style('css/jquery/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}
            {{ HTML::style('css/jquery/plugins/toastr/toastr.min.css') }}
            {{ HTML::style('css/jquery/plugins/footable/footable.core.css') }}
            {{ HTML::style('css/core/main.css') }}
            {{ HTML::script('js/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') }}
            {{ HTML::script('js/jquery/core/jquery-1.11.1.min.js') }}
            {{ HTML::script('js/jquery/plugins/bootstrap/bootstrap.min.js') }}
            {{ HTML::script('js/jquery/plugins/select2/select2.min.js') }}
            {{ HTML::script('js/jquery/plugins/select2/select2_locale_es.js') }}
            {{ HTML::script('js/moment/moment.min.js') }}
            {{ HTML::script('js/jquery/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}
            {{ HTML::script('js/jquery/plugins/footable/footable.js') }}
            {{ HTML::script('js/jquery/plugins/footable/footable.paginate.js') }}
            {{ HTML::script('js/jquery/plugins/toastr/toastr.min.js') }}
		<style>
		
		.bootstrap-tagsinput { width: 100%; }
		.trash { color:rgb(209, 91, 71); }
		.flag { color:rgb(248, 148, 6); }
		.panel-body { padding:0px; }
		.panel-footer .pagination { margin: 0; }
		.panel .glyphicon,.list-group-item .glyphicon { margin-right:5px; }
		.panel-body .radio, .checkbox { display:inline-block;margin:0px; }
		.panel-body input[type=checkbox]:checked + label { text-decoration: line-through;color: rgb(128, 144, 160); }
		.list-group-item:hover, a.list-group-item:focus {text-decoration: none;background-color: rgb(245, 245, 245);}
		.list-group { margin-bottom:0px; }
		.panel-body .checkbox{
			margin-left: 20px !important;
		}
		</style>
		<script type="text/javascript">
			$(document).ready(function(){
			    toastr.options = {
			      "closeButton": true,
			      "debug": false,
			      "positionClass": "toast-bottom-full-width",
			      "showDuration": "300",
			      "hideDuration": "1000",
			      "timeOut": "5000",
			      "extendedTimeOut": "2000",
			      "showEasing": "swing",
			      "hideEasing": "linear",
			      "showMethod": "fadeIn",
			      "hideMethod": "fadeOut"
			    }

			    @if(Session::get('mensaje'))
			    	toastr.success(' {{ Session::get('mensaje') }} ');
			    @endif

			    @if($errors->has())
			    	@foreach ($errors->all() as $error)
			    		toastr.error(' {{ $error }} ');
			    	@endforeach
			    @endif
			});
		</script>
	</head>
<body>
<div class="container">

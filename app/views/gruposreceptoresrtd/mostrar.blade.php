@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del grupo de receptores de RTD</legend>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Nombre:</th>
                            <td>{{ $gruporeceptoresrtd->nombre }}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Miembros:</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($gruporeceptoresrtd->receptoresrtd as $receptorrtd) 
                        <tr><td>{{ $receptorrtd->nombre }}</td></tr>
                        @endforeach
                    </tbody>
                </table>
        <a href="{{ URL::to('gruposreceptoresrtd') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
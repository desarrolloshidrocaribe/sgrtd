@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('route' => 'gruposreceptoresrtd.store')) }}
    <legend>Nuevo grupo de receptores de RTD</legend>
    <div class="col-md-12">
        {{ Form::label('nombre', 'Nombre'); }}
        {{ Form::text('nombre', Input::old('nombre'), ['class' => 'form-control']); }}
    </div>
    
    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('gruposreceptoresrtd') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Crear', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Gesti&oacute;n de Grupos de Receptores de RTD</legend>
        <a class='btn btn-primary btn-fixed' href="gruposreceptoresrtd/create"><span class="glyphicon glyphicon-plus-sign"></span> Nuevo</a></p>
        <table class="table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($gruposreceptoresrtd as $gruporeceptoresrtd)
            <tr>
                <td><a href="gruposreceptoresrtd/{{ $gruporeceptoresrtd->id }}">{{ $gruporeceptoresrtd->nombre }}</a></td>
                <td>
                    <div class="action-buttons">
                        {{ Form::open(array('method'=>'DELETE','route'=>array('gruposreceptoresrtd.destroy',$gruporeceptoresrtd->id))) }}
                            <a href="gruposreceptoresrtd/{{ $gruporeceptoresrtd->id }}/edit" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
                            <button type="submit" class="btn-glyphicon trash" title="Eliminar" onclick="return confirm('Esta seguro que desea eliminar este elemento?') ? true : false"    >
                                <span class="glyphicon glyphicon-trash">
                            </button>
                        {{ Form::close() }}
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </div>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="paging">
                    <ul class="pagination pagination-sm"></ul>
                </div>
        </td>
    </tr>
</tfoot>
</table>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('table').footable();
});
</script>
@stop
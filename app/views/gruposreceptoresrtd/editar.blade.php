@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('method' => 'put', 'route' => array('gruposreceptoresrtd.update', $gruporeceptoresrtd->id))) }}
    <legend>Editar grupo de receptores de RTD</legend>
    {{ Form::hidden('id', $gruporeceptoresrtd->id) }}
    <div class="col-md-12">
        {{ Form::label('nombre', 'Nombre'); }}
        {{ Form::text('nombre', Input::old('nombre') ? Input::old('nombre') : $gruporeceptoresrtd->nombre, ['class' => 'form-control']); }}
    </div>

    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('gruposreceptoresrtd') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
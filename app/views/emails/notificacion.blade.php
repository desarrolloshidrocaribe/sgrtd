<!DOCTYPE html>
<html lang="es-VE">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Nuevo RTD</h3>
		<div>
			<p>Estimado, {{ $nombre }}</p>
			<p>Se ha remitido un RTD a una dependencia de la cual usted es responsable</p>
			<p>Para visualizarlo haga click <a href="http://10.10.1.89/sgrtd/public/rtds/{{ $rtd }}">aquí</a> </p><br/>
			<p>Por favor no responda este mensaje ni lo utilice como vía de comunicación para realizar consultas; el mismo ha sido generado automaticamente a través de una cuenta no monitoreada.<p/>
			<p>C.A. Hidrologica del Caribe.</p>
		</div>
	</body>
</html>
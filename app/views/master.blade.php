@include('header')
<div class="row affix-row">
  <div class="col-sm-3 col-md-2 affix-sidebar">
    <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="visible-xs navbar-brand">Sidebar menu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
          <ul class="nav navbar-nav" id="sidenav01">
            <li class="active " >
              <a href="#" data-toggle="collapse">
                <h4>SGRTD
                  <br>
                  <small>Panel de control</small>
                </h4>
              </a>
            </li>
            @if(Auth::user()->nivelacceso == Usuario::ADMINISTRADOR)
            <li><a href="#"><span class="glyphicon glyphicon-list"></span> Monitor de RTDs</a></li>
            <li><a href="{{ URL::to('rtds/create') }}"><span class="glyphicon glyphicon-file"></span> Nuevo RTD</a></li>
            <li>
              <a href="#" data-toggle="collapse" data-target="#toggleDemo" data-parent="#sidenav01" class="collapsed">
                <span class="glyphicon glyphicon-cog"></span> Configuraci&oacute;n<span class="caret pull-right"></span>
              </a>
              <div class="collapse" id="toggleDemo" style="height: 0px;">
                <ul class="nav nav-list">
                  <li><a href="{{ URL::to('usuarios') }}">Gesti&oacute;n de usuarios</a></li>
                  <li><a href="{{ URL::to('receptoresrtds') }}">Gesti&oacute;n de receptores de RTD</a></li>
                  <li><a href="{{ URL::to('receptorespunto') }}">Gesti&oacute;n de receptores de punta de cuenta</a></li>
                  <li><a href="{{ URL::to('gruposreceptoresrtd') }}">Gesti&oacute;n de grupos de receptores</a></li>
                  <li><a href="{{ URL::to('instrucciones') }}">Gesti&oacute;n de intrucciones</a></li>
                  <li><a href="{{ URL::to('tiposdocumento') }}">Gesti&oacute;n de tipos de documentos</a></li>
                  <li><a href="{{ URL::to('notificaciones') }}">Gesti&oacute;n de notificaciones</a></li>
                </ul>
              </div>
            </li>
            @endif
          <li><a href="{{ URL::to('salir') }}"><span class="glyphicon glyphicon-off"></span> Salir</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <div class="col-sm-9 col-md-10 affix-content">
  	<div class="container">
  		@yield('content')
  	</div>
  </div>
</div>
@include('footer')
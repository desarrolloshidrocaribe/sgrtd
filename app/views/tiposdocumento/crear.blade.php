@extends('master')
@section('content')
<div class="row">
    {{ Form::open(array('route' => 'tiposdocumento.store')) }}
    <legend>Nuevo tipo de documento</legend>
    <div class="col-md-12">
        {{ Form::label('descripcion', 'Descripción'); }}
        {{ Form::text('descripcion', Input::old('descripcion'), ['class' => 'form-control']); }}
    </div>
    
    <div class="col-md-5 col-md-offset-4">
        <div class="btn-group btn-group-form" >
            <a href="{{ URL::to('tiposdocumento') }}" class='btn btn-danger btn-fixed' >Volver</a>
            {{ Form::submit('Crear', ['class' => 'btn btn-success btn-fixed']); }}
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop
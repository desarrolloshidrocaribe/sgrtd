@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Gesti&oacute;n de Tipos de documento</legend>
        <a class='btn btn-primary btn-fixed' href="tiposdocumento/create"><span class="glyphicon glyphicon-plus-sign"></span> Nuevo</a></p>
        <table class="table">
        <thead>
            <tr>
                <th>Descripci&oacute;n</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tiposdocumento as $tipodocumento)
            <tr>
                <td><a href="tiposdocumento/{{ $tipodocumento->id }}">{{ $tipodocumento->descripcion }}</a></td>
                <td>
                    <div class="action-buttons">
                        {{ Form::open(array('method'=>'DELETE','route'=>array('tiposdocumento.destroy',$tipodocumento->id))) }}
                            <a href="tiposdocumento/{{ $tipodocumento->id }}/edit" title="Editar"><span class="glyphicon glyphicon-pencil"></span></a>
                            <button type="submit" class="btn-glyphicon trash" title="Eliminar" onclick="return confirm('Esta seguro que desea eliminar este elemento?') ? true : false"    >
                                <span class="glyphicon glyphicon-trash">
                            </button>
                        {{ Form::close() }}
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </div>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="paging">
                    <ul class="pagination pagination-sm"></ul>
                </div>
        </td>
    </tr>
</tfoot>
</table>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('table').footable();
});
</script>
@stop
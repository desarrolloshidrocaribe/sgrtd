@extends('master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <legend>Informaci&oacute;n del tipo de documento</legend>
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Descripci&oacute;n:</th>
                            <td>{{ $tipodocumento->descripcion }}</td>
                        </tr>
                    </tbody>
                </table>
        <a href="{{ URL::to('tiposdocumento') }}" class="btn btn-danger" >Volver</a>
    </div>
</div>
@stop
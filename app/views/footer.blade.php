
<script type="text/javascript">

$(document).ready(function()
{
    var navItems = $('.admin-menu li > a');
    var navListItems = $('.admin-menu li');
    var allWells = $('.admin-content');
    var allWellsExceptFirst = $('.admin-content:not(:first)');
    
    allWellsExceptFirst.hide();
    navItems.click(function(e)
    {
        e.preventDefault();
        navListItems.removeClass('active');
        $(this).closest('li').addClass('active');
        
        allWells.hide();
        var target = $(this).attr('data-target-id');
	if(target)
	        $('#' + target).show();
	else
		window.location.href =  $(this).attr('href');
    });


	$('#datetimepicker2, #datetimepicker3').datetimepicker({
                    pickTime: false,
	            language: 'es'
        });

	$("#receptoresrtd, #instrucciones, #puntocuentainformacion").select2({
		allowClear: true,
	});

});

</script>
</div>
</body>
</html>
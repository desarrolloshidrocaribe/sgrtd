<?php

class TablaUsuariosSeeder extends Seeder {
	public function run() {
		Usuario::create(array(
			'nombres' => 'Javier',
			'apellidos' => 'Salazar',
			'usuario' => 'admin',
			'password' => Hash::make('admin'),
			'email' => 'jsalazar@hidrocaribe.com.ve',
			'telefonomovil' => '04263827546',
			'nivelacceso' => Usuario::ADMINISTRADOR
		));
	}
}
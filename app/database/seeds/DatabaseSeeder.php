<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('TablaUsuariosSeeder');
        $this->call('TablaTiposDocumentoSeeder');
		$this->call('TablaGruposReceptoresRTDSeeder');
        $this->call('TablaReceptoresRTDSeeder');
		$this->call('TablaInstruccionesSeeder');
		$this->call('TablaReceptoresPuntoSeeder');

	}

}

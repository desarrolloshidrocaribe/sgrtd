<?php

class TablaTiposDocumentoSeeder extends Seeder {

	public function run() {
		TipoDocumento::create(array(
			'descripcion' => 'Oficio'
		));

		TipoDocumento::create(array(
			'descripcion' => 'Comunicación Interna'
		));

		TipoDocumento::create(array(
			'descripcion' => 'Memorandum'
		));

		TipoDocumento::create(array(
			'descripcion' => 'Punto de cuenta'
		));

		TipoDocumento::create(array(
			'descripcion' => 'Punto de Información'
		));

		TipoDocumento::create(array(
			'descripcion' => 'Fax'
		));
	}

}

<?php

class TablaReceptoresRTDSeeder extends Seeder {

	public function run() {

		ReceptorRTD::create(array(
			'nombre' => 'Junta Directiva',
			'id_usuario' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Auditoria Interna',
			'id_usuario' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Consultoria Juridica',
			'id_usuario' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Gestión Comunitaria',
			'id_usuario' => 1,
			'id_grupo' => 1
		));

		ReceptorRTD::create(array(
			'nombre' =>  'Gestión de Despacho',
			'id_usuario' => 1,
			'id_grupo' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Imagen',
			'id_usuario' => 1,
			'id_grupo' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Procura',
			'id_usuario' => 1,
			'id_grupo' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Proyectos e Infraestructura',
			'id_usuario' => 1,
			'id_grupo' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Seguridad Integral',
			'id_usuario' => 1,
			'id_grupo' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Tecnología de Información',
			'id_usuario' => 1,
			'id_grupo' => 1
		));

		ReceptorRTD::create(array(
			'nombre' => 'Administración y Finanzas',
			'id_usuario' => 1,
			'id_grupo' => 2
		));

		ReceptorRTD::create(array(
			'nombre' => 'Comercialización',
			'id_usuario' => 1,
			'id_grupo' => 2
		));

		ReceptorRTD::create(array(
			'nombre' => 'Desarrollo Organizacional',
			'id_usuario' => 1,
			'id_grupo' => 2
		));

		ReceptorRTD::create(array(
			'nombre' => 'Mantenimiento',
			'id_usuario' => 1,
			'id_grupo' => 2
		));

		ReceptorRTD::create(array(
			'nombre' => 'Planificación y Control',
			'id_usuario' => 1,
			'id_grupo' => 2
		));

		ReceptorRTD::create(array(
			'nombre' => 'Producción y Tratamiento',
			'id_usuario' => 1,
			'id_grupo' => 2
		));

		ReceptorRTD::create(array(
			'nombre' => 'Técnica Operativa',
			'id_usuario' => 1,
			'id_grupo' => 2
		));

		ReceptorRTD::create(array(
			'nombre' => 'UG Anzoátegui Norte',
			'id_usuario' => 1,
			'id_grupo' => 3
		));

		ReceptorRTD::create(array(
			'nombre' => 'UG Anzoátegui Sur',
			'id_usuario' => 1,
			'id_grupo' => 3
		));

		ReceptorRTD::create(array(
			'nombre' => 'UG Nueva Esparta',
			'id_usuario' => 1,
			'id_grupo' => 3
		));

		ReceptorRTD::create(array(
			'nombre' => 'UG Sucre Oeste',
			'id_usuario' => 1,
			'id_grupo' => 3
		));

		ReceptorRTD::create(array(
			'nombre' => 'Anzoátegui Oeste',
			'id_usuario' => 1,
			'id_grupo' => 4
		));

		ReceptorRTD::create(array(
			'nombre' => 'Sucre Este',
			'id_usuario' => 1,
			'id_grupo' => 4
		));
	}
}

<?php

class TablaGruposReceptoresRTDSeeder extends Seeder {

	public function run(){
		GrupoReceptoresRTD::create(array(
			'nombre' => 'Gerencias de Área'
		));

		GrupoReceptoresRTD::create(array(
			'nombre' => 'Gerencias Corporativas UC'
		));

		GrupoReceptoresRTD::create(array(
			'nombre' => 'Gerencias Corporativas UG'
		));

		GrupoReceptoresRTD::create(array(
			'nombre' => 'Superintendencias UG'
		));
		
		
	}
}

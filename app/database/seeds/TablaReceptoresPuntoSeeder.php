<?php

class TablaReceptoresPuntoSeeder extends Seeder {

	public function run() {
		ReceptorPunto::create(array(
			'nombre' => 'Asamblea de Accionistas'
		));

		ReceptorPunto::create(array(
			'nombre' => 'Directorio de Hidroven'
		));

		ReceptorPunto::create(array(
			'nombre' => 'Junta Directiva Hidrocaribe'
		));
	}


}

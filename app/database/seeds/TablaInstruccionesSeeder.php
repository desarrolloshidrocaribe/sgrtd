<?php

class TablaInstruccionesSeeder extends Seeder {

	public function run() {
		Instruccion::create(array(
			'descripcion' => 'Para su cumplimiento'
		));

		Instruccion::create(array(
                        'descripcion' => 'Para su información y fines consiguientes'
                ));

		Instruccion::create(array(
                        'descripcion' => 'Preparar respuesta para la firma del presidente'
                ));

		Instruccion::create(array(
                        'descripcion' => 'Investigar, revisar e informarme'
                ));

		Instruccion::create(array(
                        'descripcion' => 'Hablarme de este asunto'
                ));

		Instruccion::create(array(
                        'descripcion' => 'Archivar'
                ));

		Instruccion::create(array(
                        'descripcion' => 'Tramitar'
                ));

		Instruccion::create(array(
                        'descripcion' => 'Ver observaciones'
                ));
	}

}

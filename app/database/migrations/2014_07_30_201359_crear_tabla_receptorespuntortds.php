<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaReceptorespuntortds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receptorespuntortds', function($tabla){
			$tabla->integer('id_rtd')->unsigned();
			$tabla->integer('id_receptorpunto')->unsigned();
			$tabla->primary(array('id_rtd', 'id_receptorpunto'));
            $tabla->foreign('id_rtd')->references('id')->on('rtds');
            $tabla->foreign('id_receptorpunto')->references('id')->on('receptorespunto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receptorespuntortds');
	}

}

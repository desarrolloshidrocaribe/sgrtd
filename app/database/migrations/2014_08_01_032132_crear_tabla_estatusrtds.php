<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEstatusrtds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			Schema::create('estatusrtds', function($tabla){
				$tabla->integer('id_rtd')->unsigned();
				$tabla->integer('id_usuario')->unsigned();
				$tabla->string('estatus', 32)->unsigned();
				$tabla->string('observacion')->nullable();
				$tabla->date('fecha');
				$tabla->primary(array('id_rtd', 'estatus', 'fecha'));
				$tabla->foreign('id_rtd')->references('id')->on('rtds');
				$tabla->foreign('id_usuario')->references('id')->on('usuarios');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estatusrtds');
	}

}

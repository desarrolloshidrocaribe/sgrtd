<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaInstruccionesrtds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('instruccionesrtds', function($tabla){
			$tabla->integer('id_rtd')->unsigned();
			$tabla->integer('id_instruccion')->unsigned();
			$tabla->primary(array('id_rtd', 'id_instruccion'));
            $tabla->foreign('id_rtd')->references('id')->on('rtds');
            $tabla->foreign('id_instruccion')->references('id')->on('instrucciones');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('instruccionesrtds');
	}

}

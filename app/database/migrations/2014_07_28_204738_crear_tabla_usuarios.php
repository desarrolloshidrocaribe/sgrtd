<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaUsuarios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function($tabla){
			$tabla->increments('id');
			$tabla->string('nombres', 32);
			$tabla->string('apellidos', 32);
			$tabla->string('usuario', 32)->unique();
			$tabla->string('password', 100);
			$tabla->string('email', 100);
			$tabla->string('telefonomovil', 16)->nullable();
			$tabla->smallInteger('nivelacceso')->unsigned();
			$tabla->rememberToken();
			$tabla->softDeletes();
			$tabla->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}

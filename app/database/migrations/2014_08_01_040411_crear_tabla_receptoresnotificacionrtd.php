<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaReceptoresnotificacionrtd extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receptoresnotificacionrtd', function($tabla) {
			$tabla->increments('id');
			$tabla->string('nombres', 64);
			$tabla->string('apellidos', 64);
			$tabla->string('email', 100)->unique();
			$tabla->string('telefonomovil', 14)->nullable()->unique();
			$tabla->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receptoresnotificacionrtd');
	}

}

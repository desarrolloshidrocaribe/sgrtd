<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaRtds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rtds', function($tabla){
			$tabla->increments('id');
			$tabla->date('fecha');
			$tabla->integer('id_tipodocumento')->unsigned()->nullable();
			$tabla->string('otrotipodocumento', 32)->nullable();
			$tabla->string('numerodocumento', 32);			
			$tabla->date('fechatramitacion');
			$tabla->date('fecharecepcion');
			//RECEPTORES RTD
			$tabla->string('otroreceptorrtd', 32)->nullable();
			//INSTRUCCIONES
			//RECEPTORES PTO CUENTA/INFORMACION
			$tabla->string('otropuntocuentainformacion', 32)->nullable();
			$tabla->string('coordinarcon', 32)->nullable();
			$tabla->string('observaciones')->nullable();
			$tabla->timestamps();
			//VISTAS RTDS
			//ESTATUS
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rtds');
	}

}

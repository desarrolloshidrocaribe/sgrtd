<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaReceptoresrtdrtds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receptoresrtdrtds', function($tabla){
			$tabla->integer('id_rtd')->unsigned();
			$tabla->integer('id_receptorrtd')->unsigned();
			$tabla->primary(array('id_rtd', 'id_receptorrtd'));
            $tabla->foreign('id_rtd')->references('id')->on('rtds');
            $tabla->foreign('id_receptorrtd')->references('id')->on('receptoresrtd');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receptoresrtdrtds');
	}

}

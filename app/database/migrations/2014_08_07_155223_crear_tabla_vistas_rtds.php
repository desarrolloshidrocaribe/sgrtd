<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaVistasRtds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vistasrtds', function($tabla){
			$tabla->integer('id_rtd')->unsigned();
			$tabla->integer('id_usuario')->unsigned();
			$tabla->datetime('fecha');
			$tabla->foreign('id_rtd')->references('id')->on('rtds');
			$tabla->foreign('id_usuario')->references('id')->on('usuarios');
			$tabla->primary(array('id_rtd', 'id_usuario'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vistasrtds');
	}

}

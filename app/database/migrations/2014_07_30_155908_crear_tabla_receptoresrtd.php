<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaReceptoresrtd extends Migration {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('receptoresrtd', function($tabla){
                        $tabla->increments('id');
                        $tabla->string('nombre', '32')->unique();
                        $tabla->integer('id_usuario')->unsigned();
                        $tabla->integer('id_grupo')->nullable()->unsigned();
                        $tabla->foreign('id_grupo')->references('id')->on('gruposreceptoresrtd');
                        $tabla->foreign('id_usuario')->references('id')->on('usuarios');
                        $tabla->softDeletes();
                        $tabla->timestamps();
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('receptoresrtd');
        }

}

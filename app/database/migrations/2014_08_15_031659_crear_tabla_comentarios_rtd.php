<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaComentariosRtd extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comentariosrtds', function($tabla){
			$tabla->increments('id');
			$tabla->string('comentario');
			$tabla->datetime('fecha');
			$tabla->integer('id_rtd')->unsigned();
			$tabla->integer('id_usuario')->unsigned();
			$tabla->foreign('id_rtd')->references('id')->on('rtds');
			$tabla->foreign('id_usuario')->references('id')->on('usuarios');
			$tabla->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comentariosrtds');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaNotificacionreceptoresrtd extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notificacionesreceptoresrtd', function($tabla){
				$tabla->integer('id_receptornotificacion')->unsigned();
				$tabla->integer('id_receptorrtd')->unsigned();
				$tabla->foreign('id_receptornotificacion')->references('id')->on('receptoresnotificacionrtd');
				$tabla->foreign('id_receptorrtd')->references('id')->on('receptoresrtd');
				$tabla->primary(array('id_receptornotificacion', 'id_receptorrtd'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notificacionesreceptoresrtd');
	}

}

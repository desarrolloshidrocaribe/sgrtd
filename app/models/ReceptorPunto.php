<?php

class ReceptorPunto extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'receptorespunto';
	protected $fillable = array('nombre');
}
<?php

class ReceptorNotificacionRTD extends Eloquent 
{
	protected $table = 'receptoresnotificacionrtd';

	public function receptoresrtd(){
		return $this->belongsToMany('ReceptorRTD', 'notificacionesreceptoresrtd', 'id_receptornotificacion', 'id_receptorrtd');
	}
}
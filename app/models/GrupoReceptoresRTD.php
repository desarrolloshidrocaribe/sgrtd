<?php

class GrupoReceptoresRTD extends Eloquent {

	protected $table = 'gruposreceptoresrtd';

	public function receptoresrtd() {
		return $this->hasMany('ReceptorRTD', 'id_grupo');
	}
}
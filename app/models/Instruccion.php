<?php

class Instruccion extends Eloquent {
	
	use SoftDeletingTrait;

	protected $table = 'instrucciones';
	protected $fillable = array('descripcion');

}

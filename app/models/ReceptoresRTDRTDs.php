<?php

class ReceptoresRTDRTDs extends Eloquent {
	protected $table = 'receptoresrtdrtds';
	public $timestamps = false;
	public $incrementing = false;
	protected $primaryKey = [
		'id_rtd', 
		'id_receptorrtd'
    ];
}

<?php

class TipoDocumento extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'tiposdocumento';

	protected $fillable = array('descripcion');

}

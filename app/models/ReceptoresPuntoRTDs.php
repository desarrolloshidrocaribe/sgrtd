<?php

class ReceptoresPuntoRTDs extends Eloquent {
	protected $table = 'receptorespuntortds';
	public $timestamps = false;
	public $incrementing = false;
	protected $primaryKey = [
        'id_rtd',
        'id_receptorpunto'
    ];
}
<?php

class InstruccionesRTDs extends Eloquent {
	protected $table = 'instruccionesrtds';
	public $timestamps = false;
	public $incrementing = false;
	protected $primaryKey = [
        'id_rtd',
        'id_instruccion'
    ];
}

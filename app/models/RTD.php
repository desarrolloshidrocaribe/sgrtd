<?php

class RTD extends Eloquent {
	protected $table = 'rtds';
	protected $fillable = array('numero', 'fecha', 'numerodocumento', 'fechatramitacion', 'fecharecepcion');
	
    public function tipodocumento(){
        return $this->belongsTo('TipoDocumento', 'id_tipodocumento');
    }

    public function instrucciones() {
        return $this->belongsToMany('Instruccion', 'instruccionesrtds', 'id_rtd', 'id_instruccion');
    }

    public function receptoresrtd() {
    	return $this->belongsToMany('ReceptorRTD', 'receptoresrtdrtds', 'id_rtd', 'id_receptorrtd');
    }

    public function receptoresptocuenta() {
    	return $this->belongsToMany('ReceptorPunto', 'receptorespuntortds', 'id_rtd', 'id_receptorpunto');
    }

    public function comentarios() {
        return $this->hasMany('ComentariosRTD', 'id_rtd', 'id');
    }
}

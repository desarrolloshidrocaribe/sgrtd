<?php

class ReceptorRTD extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'receptoresrtd';
	
	public function grupo() {   
        return $this->belongsTo('GrupoReceptoresRTD', 'id_grupo');
    }

    public function responsable() {
    	return $this->belongsTo('Usuario', 'id_usuario');
    }

    public function receptoresnotificacion() {
        return $this->belongsToMany('ReceptorNotificacionRTD', 'notificacionesreceptoresrtd', 'id_receptorrtd','id_receptornotificacion');
    }
}
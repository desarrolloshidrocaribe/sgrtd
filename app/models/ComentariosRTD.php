<?php

class ComentariosRTD extends Eloquent 
{
	protected $table = 'comentariosrtds';

	public function autor(){
		return $this->belongsTo('Usuario', 'id_usuario');
	}

}
<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, SoftDeletingTrait;

	const USUARIO = 0;
	const ADMINISTRADOR = 1;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	//protected $dates = ['deleted_at'];

	protected $fillable = array('nombres', 'apellidos', 'usuario', 'email', 'telefonomovil', 'nivelacceso');

	public function receptor() {
		return $this->hasMany('ReceptorRTD', 'id_usuario');
	}

}

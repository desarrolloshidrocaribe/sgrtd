<?php

class NotificacionReceptoresRTD extends Eloquent 
{
	protected $table = "notificacionesreceptoresrtd";
	public $timestamps = false;
	public $incrementing = false;
	protected $primaryKey = [
        'id_receptornotificacion',
        'id_receptorrtd'
    ];
}
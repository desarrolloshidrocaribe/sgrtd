<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('ingresar', 'SesionController@create');
Route::resource('sesion', 'SesionController');

Route::group(array('before' => 'auth'), function()
{
    Route::resource('rtds', 'RTDController');
    Route::resource('usuarios', 'UsuarioController');
    Route::resource('receptoresrtds', 'ReceptorRTDController');
    Route::resource('instrucciones', 'InstruccionController');
    Route::resource('tiposdocumento', 'TipoDocumentoController');
    Route::resource('gruposreceptoresrtd', 'GrupoReceptoresRTDController');
    Route::resource('receptorespunto', 'ReceptorPuntoController');
    Route::resource('notificaciones', 'ReceptorNotificacionRTDController');
    
    
    // Esta será nuestra ruta de bienvenida.
    Route::get('/', 'RTDController@index');
	
	// Esta ruta nos servirá para cerrar sesión.
    Route::get('salir', 'SesionController@destroy');
});
<?php

class ReceptorRTDController extends BaseController {


    public function create() 
    {
        return View::make("receptoresrtds/crear");
    }

    public function index()
    {
        $receptoresrtds = ReceptorRTD::all();
        return View::make("receptoresrtds/listar", array("receptoresrtds" => $receptoresrtds));
    }

    public function edit($id) 
    {
    	$receptorrtd = ReceptorRTD::find($id);
        if($receptorrtd) {
            return View::make("receptoresrtds/editar", array("receptorrtd" => $receptorrtd));
        }else {
            return Redirect::to('receptoresrtds')->withErrors('Ocurrio un error al intentar editar el receptor RTD.');
        }
    }

    public function store()
    {

        $validador = $this->validate(Input::all(), true);

        if ($validador->fails())
        {
            return Redirect::to('receptoresrtds/create')->withErrors($validador)->withInput(Input::all());
        }

        $receptorRTD = new ReceptorRTD();
        $receptorRTD->nombre = Input::get('nombre');
        if(Input::get('grupo') && Input::get('grupo') != -1){
            $receptorRTD->id_grupo = Input::get('grupo');    
        }
        $receptorRTD->id_usuario = Input::get('responsable');

        if($receptorRTD->save()){
            return Redirect::to('receptoresrtds/create')->with(array('mensaje' => 'El receptor RTD ha sido creado correctamente.'));
        }else {
            return Redirect::to('receptoresrtds')->withErrors('Ocurrio un error al intentar crear el receptor RTD.');
        }
    }

    public function show($id) 
    {

		$receptorrtd = ReceptorRTD::find($id);

        if(!$receptorrtd)
        {
            return Redirect::to('receptoresrtds')->withErrors('Ocurrio un error al intentar mostrar el receptor de RTD.');
        }

        return View::make("receptoresrtds/mostrar", array("receptorrtd" => $receptorrtd));			
    }

    public function update($id) 
    {
        $receptorrtd = ReceptorRTD::find($id);

        if(!$receptorrtd) {
            return Redirect::to('receptoresrtds')->withErrors('Ocurrio un error al intentar editar el receptor RTD.');
        }

        $validador = $this->validate(Input::all());

        if ($validador->fails())
        {
            return Redirect::to('receptoresrtds/'.$id.'/edit/')->withErrors($validador)->withInput(Input::all());
        }

        $receptorrtd->nombre = Input::get('nombre');
        if(Input::get('grupo'))
        {
            if(Input::get('grupo') == -1){
                $receptorrtd->id_grupo = null;
            }else {
                $receptorrtd->id_grupo = Input::get('grupo');
            }
            
        }
        $receptorrtd->id_usuario = Input::get('responsable');

        if($receptorrtd->save()) {
            return Redirect::to('receptoresrtds')->with(array('mensaje' => 'El receptor de RTD ha sido editado correctamente.'));
        }else {
            return Redirect::to('receptoresrtds/'.$id.'/edit/')->withErrors('Ocurrio un error al intentar editar el receptor RTD.');
        }
    }

    public function destroy($id) 
    {
        $receptorrtd = ReceptorRTD::find($id);

        if(!$receptorrtd) 
        {
            return Redirect::to('receptoresrtds')->withErrors('Ocurrio un error al intentar eliminar el receptor RTD.');
        }

        if($receptorrtd->delete())
        {
            return Redirect::to('receptoresrtds')->with(array('mensaje' => 'El receptor de RTD ha sido eliminado correctamente.'));
        }
        else 
        {
            return Redirect::to('receptoresrtds')->withErrors('Ocurrio un error al intentar eliminar el receptor RTD.');
        }
    }

    private function validate($inputs = array(), $registro = false)
    {

        $reglas = array(
            'nombre'         => array('required','min:3','max:32'),
            'responsable'    => array('required')
        );

        if($registro) {
            $reglas['nombre'][] = 'unique:receptoresrtd,nombre';
        }else {
            $reglas['nombre'][] = 'unique:receptoresrtd,nombre,'.$inputs['id'];
        }
        
        return Validator::make($inputs, $reglas);
    }
}
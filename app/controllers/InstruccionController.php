<?php

class InstruccionController extends BaseController {


    public function create() {
        return View::make("instrucciones/crear");
    }

    public function index()
    {
        $instrucciones = Instruccion::all();
        return View::make("instrucciones/listar", array("instrucciones" => $instrucciones));
    }

    public function edit($id) 
    {
    	$instruccion = Instruccion::find($id);

		if(!$instruccion)
    	{
            return Redirect::to('instrucciones/'.$id.'/edit')->withErrors('Ocurrio un error al intentar editar la instruccion');
    	}

        return View::make("instrucciones/editar", array("instruccion" => $instruccion));		
    	
    }

    public function store(){

        $validador = $this->validate(Input::all(), true);

        if ($validador->fails())
        {
            return Redirect::to('instrucciones/create')->withErrors($validador)->withInput(Input::all());
        }

        $instruccion = new Instruccion();
        $instruccion->descripcion = Input::get('descripcion');
        $usuario->save();

        return Redirect::to('instrucciones/create')->with(array('mensaje' => 'La instrucción ha sido creada correctamente.'));
    }

    public function show($id) {
		$instruccion = Instruccion::find($id);

        if(!$instruccion)
        {
            return Redirect::to('instrucciones')->withErrors('Ocurrio un error al intentar mostrar la instruccion');
        }

        return View::make("instrucciones/mostrar", array("instruccion" => $instruccion));
    }

    public function update($id) 
    {
        $instruccion = Instruccion::find($id);

        if(!$instruccion)
        {
            return Redirect::to('instrucciones')->withErrors('Ocurrio un error al intentar editar la instruccion')->withInput(Input::all());;
        }

        $validador = $this->validate(Input::all());

        if ($validador->fails())
        {
            return Redirect::to('instrucciones/create')->withErrors($validador)->withInput(Input::all());
        }


        $instruccion->descripcion = Input::get('descripcion');

        if($instruccion->save()) {
            return Redirect::to('instrucciones')->with(array('mensaje' => 'La instruccion ha sido editada correctamente.'));
        }else {
            return Redirect::to('instrucciones')->withErrors('Ocurrio un error al intentar editar la instruccion')->withInput(Input::all());;
        }
    }

    public function destroy($id) {
        $instruccion = Instruccion::find($id);

        if(!$instruccion)
        {
            return Redirect::to('instrucciones')->withErrors('Ocurrio un error al intentar eliminar la instruccion')->withInput(Input::all());;
        }

        if($instruccion->delete()) {
            return Redirect::to('instrucciones')->with(array('mensaje' => 'La instruccion ha sido eliminada correctamente.'));
        }else {
            return Redirect::to('instrucciones')->withErrors('Ocurrio un error al intentar eliminar la instruccion')->withInput(Input::all());;
        }
    }

    private function validate($inputs = array(), $registro = false)
    {

        $reglas = array(
            'descripcion'         => array('required','min:3','max:100')
        );

        if($registro) {
            $reglas['descripcion'][] = 'unique:instrucciones,descripcion';
        }else {
            $reglas['descripcion'][] = 'unique:instrucciones,descripcion,'.$inputs['id'];
        }
        
        return Validator::make($inputs, $reglas);
    }
}
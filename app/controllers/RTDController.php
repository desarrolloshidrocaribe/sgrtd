<?php

class RTDController extends BaseController {


    public function create() {
        $fecha = date('d/m/Y');
        $grupos = GrupoReceptoresRTD::with('receptoresRTD')->get();
        return View::make("rtds/crear", array('fecha' => $fecha, 'grupos' => $grupos));
    }

    public function index()
    {
        $rtds = RTD::all();
        return View::make("rtds/listar", array("rtds" => $rtds));
    }

    public function edit($id) 
    {
    	return Redirect::to('rtds')->withErrors('No esta permitida la edicion');
    }

    public function store(){
        
        $validador = $this->validate(Input::all());

        if($validador->fails()) {
            return Redirect::to('rtds/create')->withErrors($validador)->withInput(Input::all());;
        }

        $rtd = new RTD();
        $rtd->fecha = Input::get('fecha');
        $rtd->id_tipodocumento = Input::get('tipodocumento');
        $rtd->otrotipodocumento = Input::get('otrotipodocumento');
        $rtd->numerodocumento = Input::get('numerodocumento');
        $rtd->fechatramitacion = Input::get('fechatramitacion');
        $rtd->fecharecepcion = Input::get('fecharecepcion');
        $rtd->otroreceptorrtd = Input::get('otroreceptorrtd');
        $rtd->otropuntocuentainformacion = Input::get('otropuntocuentainformacion');
        $rtd->coordinarcon = Input::get('coordinarcon');
        $rtd->observaciones = Input::get('observaciones');


        $error = false;
        DB::transaction(function() use ($rtd)
        {
            if($rtd->save()) {

                if(Input::has('receptoresrtd')) {
                    foreach (Input::get('receptoresrtd') as $idReceptorRTD) 
                    {
                        $receptoresRTDRTDs = new ReceptoresRTDRTDs();
                        $receptoresRTDRTDs->id_rtd = $rtd->id;
                        $receptoresRTDRTDs->id_receptorrtd = $idReceptorRTD;
                        $error = !$receptoresRTDRTDs->save();
                    }
                }

                if(Input::has('instrucciones')) {
                    foreach (Input::get('instrucciones') as $idInstruccion) 
                    {
                        $instruccionesRTDs = new InstruccionesRTDs();
                        $instruccionesRTDs->id_rtd = $rtd->id;
                        $instruccionesRTDs->id_instruccion = $idInstruccion;
                        $error = !$instruccionesRTDs->save();
                    }
                }

                if(Input::has('puntocuentainformacion')) {
                    foreach (Input::get('puntocuentainformacion') as $idPuntoCuentaInformacion) 
                    {
                        $receptoresPuntoRTDs = new ReceptoresPuntoRTDs();
                        $receptoresPuntoRTDs->id_rtd = $rtd->id;
                        $receptoresPuntoRTDs->id_receptorpunto = $idPuntoCuentaInformacion;
                        $error = !$receptoresPuntoRTDs->save();
                    }
                }
            }

        });

        if($error) 
        {
            return Redirect::to('rtds/create')->withErrors('Ocurrio un error inesperado al intentar crear el RTD.')->withInput(Input::all());
        }
        else
        {
            $destinatarios = array();
            foreach ($rtd->receptoresrtd as $ireceptorrtd) {
                $receptorrtd = ReceptorRTD::find($ireceptorrtd->id);
                $destinatarios[] = array(
                            'nombre' => $receptorrtd->responsable->nombres.' '.$receptorrtd->responsable->apellidos, 
                            'email'  => $receptorrtd->responsable->email,
                            'movil' =>  $receptorrtd->responsable->telefonomovil);
                foreach ($receptorrtd->receptoresnotificacion as  $receptornotificacion) {
                    $destinatarios[] = array(
                        'nombre' => $receptornotificacion->nombres.' '.$receptornotificacion->apellidos, 
                        'email'  => $receptornotificacion->email,
                        'movil'  => $receptornotificacion->telefonomovil);
                }
            }

            foreach ($destinatarios as $destinatario) 
            {
                $data = array('rtd' => $rtd->id, 'nombre' => $destinatario['nombre']);
                
                Mail::queue('emails.notificacion', $data, function($mensaje) use($destinatario)
                {
                    $mensaje->from('rtd@hidrocaribe.com.ve', 'SGRTD');
                    $mensaje->to($destinatario['email'], $destinatario['nombre'])->subject('Nuevo RTD');
                });

                Playsms::sendSms($destinatario['movil'], "Un nuevo RTD se ha generado, por favor ingrese a la aplicacion para visualizarlo. - C.A. Hidrologica del Caribe.");
            }

            return Redirect::to('rtds/create')->with(array('mensaje' => '<p style="text-align:center;"> El RTD ha sido creado correctamente con  nro <h2 style="text-align:center;">'.str_pad($rtd->id, 3, '0', STR_PAD_LEFT)."</h2></p>"));
        }
    }

    public function show($id) {
		$rtd = RTD::find($id);
        $tiposdocumento = TipoDocumento::all();

		if($rtd){
			return View::make("rtds/mostrar", array('rtd' => $rtd));			
		}
    }

    private function validate($inputs = array())
    {

        $reglas = array(
                'fecha'                         => array('required',),
                'tipodocumento'                 => array('required_without:otrotipodocumento'),
                'otrotipodocumento'             => array('required_without:tipodocumento', 'min:3','max:32'),
                'numerodocumento'               => array('required','min:3','max:32'),
                'fechatramitacion'              => array('required',),
                'fecharecepcion'                => array('required',),
                'receptoresrtd'                 => array('required_without:otroreceptorrtd'),
                'otroreceptorrtd'               => array('required_without:receptoresrtd','min:3','max:32'),
                'instrucciones'                 => array('required'),
                'otropuntocuentainformacion'    => array('min:3','max:32'),
                'coordinarcon'                  => array('alpha','min:3','max:32'),
                'observaciones'                 => array('min:3','max:255')
        );
        
        return Validator::make($inputs, $reglas);
    }
}

   

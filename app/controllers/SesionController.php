<?php

class SesionController extends BaseController {

	public function index() {
		return Redirect::to('/');
	}

	public function create() {

	        // Verificamos que el usuario no esté autenticado
	        if (Auth::check())
        	{
	            // Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
        	    return Redirect::to('/');
	        }
        	// Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)
	        return View::make('ingresar');
	}

	public function store() {

	        // Guardamos en un arreglo los datos del usuario.
        	$userdata = array(
	            'usuario' => Input::get('usuario'),
        	    'password'=> Input::get('password')
	        );

 	       // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
	        if(Auth::attempt($userdata, Input::get('recordarme', 0)))
	        {
        	    // De ser datos válidos nos mandara a la bienvenida
	            return Redirect::intended('/');

        	}

	        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
        	return Redirect::to('ingresar')
                	    ->with('mensaje_error', 'Tus datos son incorrectos')
	                    ->withInput();
	}

	public function destroy()
	{
		Auth::logout();
        Session::flush();
        return Redirect::to('ingresar')
				->with('mensaje_error', 'Tu sesión ha sido cerrada.');
	}
}

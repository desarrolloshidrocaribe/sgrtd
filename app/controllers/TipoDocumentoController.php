<?php

class TipoDocumentoController extends BaseController {

	public function index() {
		return View::make('tiposdocumento/listar', array('tiposdocumento' => TipoDocumento::all()));
	}

	public function create() {
		return View::make('tiposdocumento/crear');
	}

	public function show($id) {
		$tipodocumento = TipoDocumento::find($id);

		if(!$tipodocumento) {
			return Redirect::to('tiposdocumento')->withErrors('Ocurrio un error al intentar mostrar el tipo de documento');
		}

		return View::make('tiposdocumento/mostrar', array('tipodocumento' => $tipodocumento));
	}

	public function store() {

		$validador = $this->validate(Input::all(), true);

		if($validador->fails())
		{
			return Redirect::to('tiposdocumento/create')->withErrors($validador)->withInput(Input::all());
		}

		$tipodocumento = new TipoDocumento();
		$tipodocumento->descripcion = Input::get('descripcion');

		if($tipodocumento->save())
		{
			return Redirect::to('tiposdocumento')->with(array('mensaje' => 'El tipo de documento ha sido creado correctamente.'));
		}
		else
		{
			return Redirect::to('tiposdocumento/create')->withErrors($validador)->withInput(Input::all());
		}
	}

	public function edit($id) {

		$tipodocumento = TipoDocumento::find($id);

		if(!$tipodocumento) {
			return Redirect::to('tiposdocumento')->withErrors('Ocurrio un error al intentar mostrar el tipo de documento');
		}

		return View::make('tiposdocumento/editar', array('tipodocumento' => $tipodocumento));
	}

	public function update($id) 
	{

		$tipodocumento = TipoDocumento::find($id);

		if(!$tipodocumento) 
		{
			return Redirect::to('tiposdocumento')->withErrors('Ocurrio un error al intentar mostrar el tipo de documento');
		}

		$validador = $this->validate(Input::all());

		if($validador->fails())
		{
			return Redirect::to('tiposdocumento/'.$id.'/edit')->withErrors($validador)->withInput(Input::all());
		}

		$tipodocumento->descripcion = Input::get('descripcion');

		if($tipodocumento->save()) 
		{
			return Redirect::to('tiposdocumento')->with(array('mensaje' => 'El tipo de documento ha sido editado correctamente.'));
		}else {
			return Redirect::to('tiposdocumento/'.$id.'/edit')->withErrors($validador)->withInput(Input::all());
		}

	}

	public function destroy($id)
	{
		$tipodocumento = TipoDocumento::find($id);

		if(!$tipodocumento) 
		{
			return Redirect::to('tiposdocumento')->withErrors('Ocurrio un error al intentar eliminar el tipo de documento');
		}

		if($tipodocumento->delete())
		{
			return Redirect::to('tiposdocumento')->with(array('mensaje' => 'El tipo de documento ha sido eliminado correctamente.'));
		}
		else
		{
			return Redirect::to('tiposdocumento')->withErrors('Ocurrio un error al intentar eliminar el tipo de documento');	
		}
	}

	private function validate($inputs = array(), $registro = false)
    {

        $reglas = array(
            'descripcion'         => array('required','min:3','max:32')
        );

        if($registro) {
            $reglas['descripcion'][] = 'unique:tiposdocumento,descripcion';
        }else {
            $reglas['descripcion'][] = 'unique:tiposdocumento,descripcion,'.$inputs['id'];
        }
        
        return Validator::make($inputs, $reglas);
    }
}
<?php

class GrupoReceptoresRTDController extends BaseController 
{
	public function index() 
	{
		return View::make('gruposreceptoresrtd/listar', array('gruposreceptoresrtd' => GrupoReceptoresRTD::all()));
	}

	public function create() 
	{
		return View::make('gruposreceptoresrtd/crear');
	}

	public function store() {
		$validador = $this->validate(Input::all(), true);

		if($validador->fails()){
			return Redirect::to('gruposreceptoresrtd/create')->withErrors($validador)->withInput(Input::all());
		}

		$gruporeceptoresrtd = new GrupoReceptoresRTD();
		$gruporeceptoresrtd->nombre = Input::get('nombre');
		if($gruporeceptoresrtd->save()) 
		{
			return Redirect::to('gruposreceptoresrtd')->with(array('mensaje' => 'El grupo de receptores de RTD ha sido creado correctamente.'));
		}
		else
		{
			return Redirect::to('gruposreceptoresrtd/create')->withErrors($validador)->withInput(Input::all());
		}

	}

	public function show($id) 
	{
		$gruporeceptoresrtd = GrupoReceptoresRTD::find($id);

		if(!$gruporeceptoresrtd) 
		{
			return Redirect::to('gruposreceptoresrtd')->withErrors('Ocurrio un error al intentar editar el grupo receptor de RTD');
		}

		return View::make('gruposreceptoresrtd/mostrar', array('gruporeceptoresrtd' => $gruporeceptoresrtd));
	}

	public function edit($id) 
	{
		$gruporeceptoresrtd = GrupoReceptoresRTD::find($id);

		if(!$gruporeceptoresrtd) 
		{
			return Redirect::to('gruposreceptoresrtd')->withErrors('Ocurrio un error al intentar editar el grupo receptor de RTD');
		}

		return View::make('gruposreceptoresrtd/editar', array('gruporeceptoresrtd' => $gruporeceptoresrtd));
	}

	public function update($id) 
	{
		$gruporeceptoresrtd = GrupoReceptoresRTD::find($id);

		if(!$gruporeceptoresrtd) 
		{
			return Redirect::to('gruposreceptoresrtd')->withErrors('Ocurrio un error al intentar editar el grupo receptor de RTD');
		}

		$validador = $this->validate(Input::all());

		if($validador->fails()){
			return Redirect::to('gruposreceptoresrtd/'.$id.'/edit')->withErrors($validador)->withInput(Input::all());
		}

		$gruporeceptoresrtd->nombre = Input::get('nombre');

		if($gruporeceptoresrtd->save()) {
			return Redirect::to('gruposreceptoresrtd')->with(array('mensaje' => 'El grupo de receptores de RTD ha sido editado correctamente.'));
		}else {
			return Redirect::to('gruposreceptoresrtd')->withErrors('Ocurrio un error al intentar editar el grupo receptor de rtd')->withInput(Input::all());
		}

	}

	public function destroy($id) {
		$gruporeceptoresrtd = GrupoReceptoresRTD::find($id);

		if(!$gruporeceptoresrtd) 
		{
			return Redirect::to('gruposreceptoresrtd')->withErrors('Ocurrio un error al intentar eliminar el grupo receptor de RTD');
		}

		if($gruporeceptoresrtd->receptoresrtd->count() > 0)
		{
			return Redirect::to('gruposreceptoresrtd')->withErrors('No es posible eliminar el grupo receptor de RTD porque tiene miembros');
		}

		if($gruporeceptoresrtd->delete()) {
			return Redirect::to('gruposreceptoresrtd')->with(array('mensaje' => 'El grupo de receptores de RTD ha sido eliminado correctamente.'));
		}
		else
		{
			return Redirect::to('gruposreceptoresrtd')->withErrors('Ocurrio un error al intentar eliminar el grupo receptor de RTD')->withInput(Input::all());
		}
	}

	private function validate($inputs = array(), $registro = false)
    {

        $reglas = array(
            'nombre'         => array('required','min:3','max:32')
        );

        if($registro) {
            $reglas['nombre'][] = 'unique:gruposreceptoresrtd,nombre';
        }else {
            $reglas['nombre'][] = 'unique:gruposreceptoresrtd,nombre,'.$inputs['id'];
        }
        
        return Validator::make($inputs, $reglas);
    }
}
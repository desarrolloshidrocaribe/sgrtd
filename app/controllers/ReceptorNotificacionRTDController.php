<?php

class ReceptorNotificacionRTDController extends BaseController 
{

	public function index() 
	{
		$receptoresnotificacionRTD = ReceptorNotificacionRTD::all();
		return View::make('receptoresnotificaciones/listar', array('receptoresnotificacionRTD' => $receptoresnotificacionRTD));
	}

	public function show($id) 
	{
		$receptornotificacionRTD = ReceptorNotificacionRTD::find($id);
		if(!$receptornotificacionRTD){
			return Redirect::to('notificaciones')->withErrors('Ocurrio un error inesperado al intentar mostrar el receptor de notificaciones.')->withInput(Input::all());
		}

		return View::make('receptoresnotificaciones/mostrar', array('receptornotificacionRTD' => $receptornotificacionRTD));
	}

	public function edit($id) 
	{
		$receptornotificacionRTD = ReceptorNotificacionRTD::find($id);

		$grupos = GrupoReceptoresRTD::with('receptoresRTD')->get();

		if(!$receptornotificacionRTD){
			return Redirect::to('notificaciones')->withErrors('Ocurrio un error inesperado al intentar mostrar el receptor de notificaciones.')->withInput(Input::all());
		}

		return View::make('receptoresnotificaciones/editar', array('receptornotificacionRTD' => $receptornotificacionRTD, 'grupos' => $grupos));
	}

	public function update($id) 
	{
		$receptornotificacionRTD = ReceptorNotificacionRTD::find($id);

		if(!$receptornotificacionRTD){
			return Redirect::to('notificaciones')->withErrors('Ocurrio un error inesperado al intentar editar el receptor de notificaciones.')->withInput(Input::all());
		}

		$validador = $this->validate(Input::all());

		if($validador->fails()){
			return Redirect::to('notificaciones/'.$id.'/edit')->withErrors($validador)->withInput(Input::all());
		}

		$receptornotificacionRTD->nombres = Input::get('nombres');
		$receptornotificacionRTD->apellidos = Input::get('apellidos');
		$receptornotificacionRTD->email = Input::get('email');
		$receptornotificacionRTD->telefonomovil = Input::get('telefonomovil');

		
		$error = false;
		DB::transaction(function() use ($receptornotificacionRTD)
		{
				foreach ($receptornotificacionRTD->receptoresrtd as $receptorrtd) {
					$notificacionreceptorrtd = NotificacionReceptoresRTD::whereid_receptornotificacion($receptornotificacionRTD->id)->whereid_receptorrtd($receptorrtd->id);
					$error = !$notificacionreceptorrtd->delete();
				}

				foreach (Input::get('receptoresrtd') as $idReceptorRTD) {
					$notificacionreceptoresrtd = new NotificacionReceptoresRTD();
					$notificacionreceptoresrtd->id_receptornotificacion = $receptornotificacionRTD->id;
					$notificacionreceptoresrtd->id_receptorrtd = $idReceptorRTD;
					$error = !$notificacionreceptoresrtd->save();
				}
		});

		if($error) 
		{
			return Redirect::to('notificaciones/'.$id.'/edit')->withErrors('Ocurrio un error inesperado al intentar editar el receptor de notificaciones.')->withInput(Input::all());
		}
		else
		{
			return Redirect::to('notificaciones')->with(array('mensaje' => 'El receptor de notificaciones ha sido editado correctamente.'));
		}
	}

	public function create() 
	{
		$grupos = GrupoReceptoresRTD::with('receptoresRTD')->get();
		return View::make('receptoresnotificaciones/crear', array('grupos' => $grupos));
	}

	public function store() 
	{
		$validador = $this->validate(Input::all(), true);

		if($validador->fails()){
			return Redirect::to('notificaciones/create')->withErrors($validador)->withInput(Input::all());
		}

		$receptornotificacionRTD = new ReceptorNotificacionRTD();
		$receptornotificacionRTD->nombres = Input::get('nombres');
		$receptornotificacionRTD->apellidos = Input::get('apellidos');
		$receptornotificacionRTD->email = Input::get('email');
		$receptornotificacionRTD->telefonomovil = Input::get('telefonomovil');

		$error = false;
		DB::transaction(function() use ($receptornotificacionRTD)
		{

			if($receptornotificacionRTD->save())
			{
				foreach (Input::get('receptoresrtd') as $idReceptorRTD) {
					$notificacionreceptoresrtd = new NotificacionReceptoresRTD();
					$notificacionreceptoresrtd->id_receptornotificacion = $receptornotificacionRTD->id;
					$notificacionreceptoresrtd->id_receptorrtd = $idReceptorRTD;
					$error = !$notificacionreceptoresrtd->save();
				}
			}
			else
			{
				$error = false;
			}
		});
		
		if($error) 
		{
			return Redirect::to('notificaciones/create')->withErrors('Ocurrio un error inesperado al intentar crear el receptor de notificaciones.')->withInput(Input::all());
		}
		else
		{
			return Redirect::to('notificaciones/create')->with(array('mensaje' => 'El receptor de notificaciones ha sido creado correctamente.'));
		}
	
	}

	public function destroy($id) {

		$receptornotificacionRTD = ReceptorNotificacionRTD::find($id);

		if(!$receptornotificacionRTD){
			return Redirect::to('notificaciones')->withErrors('Ocurrio un error inesperado al intentar eliminar el receptor de notificaciones.')->withInput(Input::all());
		}

		$error = false;
		DB::transaction(function() use ($receptornotificacionRTD)
		{
			foreach ($receptornotificacionRTD->receptoresrtd as $receptorrtd) {
				$notificacionreceptorrtd = NotificacionReceptoresRTD::whereid_receptornotificacion($receptornotificacionRTD->id)->whereid_receptorrtd($receptorrtd->id);
				$error = !$notificacionreceptorrtd->delete();
			}

			$error = !$receptornotificacionRTD->delete();

		});

		if($error) 
		{
			return Redirect::to('notificaciones')->withErrors('Ocurrio un error inesperado al intentar eliminar el receptor de notificaciones.')->withInput(Input::all());
		}
		else
		{
			return Redirect::to('notificaciones')->with(array('mensaje' => 'El receptor de notificaciones ha sido eliminado correctamente.'));
		}
	}

	private function validate($inputs = array(), $registro = false)
    {

        $reglas = array(
                'nombres'        => array('required','alpha','min:2','max:32'),
                'apellidos'      => array('required','alpha','min:2','max:32'),
                'email'          => array('required','email', 'max:100'),
                'telefonomovil'  => array('min:11','max:12'),
                'receptoresrtd'  => array('required')
        );

        if($registro) {
            $reglas['email'][] = 'unique:receptoresnotificacionrtd,email';
            $reglas['telefonomovil'][] = 'unique:receptoresnotificacionrtd,telefonomovil';
        }else {
            $reglas['email'][] = 'unique:receptoresnotificacionrtd,email,'.$inputs['id'];
            $reglas['telefonomovil'][] = 'unique:receptoresnotificacionrtd,email,'.$inputs['id'];
        }
        
        return Validator::make($inputs, $reglas);
    }
	
}
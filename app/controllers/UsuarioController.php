<?php

class UsuarioController extends BaseController {


    public function create() 
    {
        return View::make("usuarios/crear");
    }

    public function index()
    {
        return View::make("usuarios/listar", array("usuarios" => Usuario::all()));
    }

    public function edit($id) 
    {
    	$usuario = Usuario::find($id);
        if($usuario){
            return View::make("usuarios/editar", array("usuario" => $usuario));    
        }else {
            return Redirect::to('usuarios')->withErrors('Ocurrio un error al intentar editar el usuario.');
        }
        
    }

    public function destroy($id) 
    {
        $usuario = Usuario::find($id);

        if($usuario)
        {

            if($usuario->nivelacceso == Usuario::ADMINISTRADOR &&
                Usuario::where('nivelacceso', '=', Usuario::ADMINISTRADOR)->count() == 1) {
                    return Redirect::to('usuarios')->withErrors('No es posible eliminar al unico usuario administrador.');
            }

            if($usuario->delete())
            {
                return Redirect::to('usuarios')->with(array('mensaje' => 'El usuario ha sido eliminado correctamente.'));
            }else {
                return Redirect::to('usuarios')->withErrors('Ocurrio un error al intentar eliminar el usuario.');
            }
        }
        else 
        {
            return Redirect::to('usuarios')->withErrors('Ocurrio un error al intentar eliminar el usuario.');
        }
    }

    public function store(){

        $validador = $this->validate(Input::all(), true);

        if ($validador->fails())
        {
            return Redirect::to('usuarios/create')->withErrors($validador)->withInput(Input::except(array('password', 'repetirpassword')));
        }

        $usuario = new Usuario();
        $usuario->nombres = Input::get('nombres');
        $usuario->apellidos = Input::get('apellidos');
        $usuario->usuario = Input::get('usuario');
        $usuario->password = Hash::make(Input::get('password'));
        $usuario->email = Input::get('email');
        $usuario->telefonomovil = Input::get('telefonomovil');
        $usuario->nivelacceso = Input::get('nivelacceso');

        if($usuario->save())
        {
            return Redirect::to('usuarios/create')->with(array('mensaje' => 'El usuario ha sido creado correctamente.'));
        }else 
        {
            return Redirect::to('usuarios/create')->withErrors('Ocurrio un error inesperado al intentar crear al usuario.')->withInput(Input::all());
        }

        
    }

    public function update($id) {

        $usuario = Usuario::find($id);

        if(!$usuario) 
        {
            return Redirect::to('usuarios')->withErrors('Ocurrio un error inesperado al intentar editar al usuario.')->withInput(Input::all());
        }

        $validador = $this->validate(Input::all(), false, $id);

        if ($validador->fails())
        {
            return Redirect::to('usuarios/'.$id.'/edit/')->withErrors($validador)->withInput(Input::except(array('password', 'password_confirmation')));
        }

        if($usuario->nivelacceso == Usuario::ADMINISTRADOR 
                && Input::get('nivelacceso') == Usuario::USUARIO
                && Usuario::where('nivelacceso', '=', Usuario::ADMINISTRADOR)->count() == 1) {
                    return Redirect::to('usuarios')->withErrors('No es posible disminuir el nivel de acceso al unico usuario administrador.');
        }

        $usuario->nombres = Input::get('nombres');
        $usuario->apellidos = Input::get('apellidos');
        $usuario->usuario = Input::get('usuario');

        if(strlen(trim(Input::get('password'))) > 0)
        {
            $usuario->password = Hash::make(Input::get('password'));
        }

        $usuario->email = Input::get('email');
        $usuario->telefonomovil = Input::get('telefonomovil');
        $usuario->nivelacceso = Input::get('nivelacceso');

        if($usuario->save())
        {
            return Redirect::to('usuarios')->with(array('mensaje' => 'El usuario ha sido editado correctamente.'));
        }
        else 
        {
            return Redirect::to('usuarios/'.$id.'/edit/')->withErrors('Ocurrio un error inesperado al intentar editar al usuario.')->withInput(Input::all());
        }
    }

    public function show($id) 
    {
        $nivelesacceso = array(Usuario::USUARIO => 'Usuario', Usuario::ADMINISTRADOR => 'Administrador');

		$usuario = Usuario::find($id);
        $usuario->nivelacceso = $nivelesacceso[$usuario->nivelacceso];

		if($usuario)
        {
			return View::make("usuarios/mostrar", array("usuario" => $usuario));			
		}else {
            return Redirect::to('usuarios')->withErrors('Ocurrio un error al intentar mostrar el usuario.');
        }
    }

    private function validate($inputs = array(), $registro = false)
    {

        $reglas = array(
                'nombres'               => array('required','alpha','min:2','max:32'),
                'apellidos'             => array('required','alpha','min:2','max:32'),
                'usuario'               => array('required','min:3','max:32'),
                'password'              => array('required_with:password_confirmation','confirmed','min:6','max:32'),
                'password_confirmation' => array('required_with:password'),
                'email'                 => array('required','email','max:100'),
                'telefonomovil'         => array('required','min:11','max:12'),
                'nivelacceso'           => array('required','max:100')
        );

        if($registro) {
            $reglas['password'][] = 'required';
            $reglas['usuario'][] = 'unique:usuarios,usuario';
        }else {
            $reglas['usuario'][] = 'unique:usuarios,usuario,'.$inputs['id'];
        }
        
        return Validator::make($inputs, $reglas);
    }
}
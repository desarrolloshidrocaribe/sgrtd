<?php

class ReceptorPuntoController extends BaseController {


    public function create() 
    {
        return View::make("receptorespunto/crear");
    }

    public function index()
    {
        $receptorespunto = ReceptorPunto::all();
        return View::make("receptorespunto/listar", array("receptorespunto" => $receptorespunto));
    }

    public function edit($id) 
    {
    	$receptorpunto = ReceptorPunto::find($id);
        if($receptorpunto) {
            return View::make("receptorespunto/editar", array("receptorpunto" => $receptorpunto));
        }else {
            return Redirect::to('receptorespunto')->withErrors('Ocurrio un error al intentar editar el receptor de punto de cuenta.');
        }
    }

    public function store()
    {

        $validador = $this->validate(Input::all(), true);

        if ($validador->fails())
        {
            return Redirect::to('receptorespunto/create')->withErrors($validador)->withInput(Input::all());
        }

        $receptorPunto = new ReceptorPunto();
        $receptorPunto->nombre = Input::get('nombre');

        if($receptorPunto->save())
        {
            return Redirect::to('receptorespunto/create')->with(array('mensaje' => 'El receptor de punto de cuenta ha sido creado correctamente.'));
        }
        else
        {
            return Redirect::to('receptorespunto')->withErrors('Ocurrio un error al intentar crear el receptor de punto de cuenta.');
        }
    }

    public function show($id) 
    {

		$receptorpunto = ReceptorPunto::find($id);

        if(!$receptorpunto)
        {
            return Redirect::to('receptorespunto')->withErrors('Ocurrio un error al intentar mostrar el receptor de punto de cuenta.');
        }

        return View::make("receptorespunto/mostrar", array("receptorpunto" => $receptorpunto));			
    }

    public function update($id) 
    {
        $receptorPunto = ReceptorPunto::find($id);

        if(!$receptorPunto) {
            return Redirect::to('receptorespunto')->withErrors('Ocurrio un error al intentar editar el receptor de punto de cuenta.');
        }

        $validador = $this->validate(Input::all());

        if ($validador->fails())
        {
            return Redirect::to('receptorespunto/'.$id.'/edit/')->withErrors($validador)->withInput(Input::all());
        }

        $receptorPunto->nombre = Input::get('nombre');

        if($receptorPunto->save()) {
            return Redirect::to('receptorespunto')->with(array('mensaje' => 'El receptor de punto de cuenta ha sido editado correctamente.'));
        }else {
            return Redirect::to('receptorespunto/'.$id.'/edit/')->withErrors('Ocurrio un error al intentar editar el receptor de punto de cuenta.');
        }
    }

    public function destroy($id) 
    {
        $receptorPunto = ReceptorPunto::find($id);

        if(!$receptorPunto) 
        {
            return Redirect::to('receptorespunto')->withErrors('Ocurrio un error al intentar eliminar el receptor de punto de cuenta.');
        }

        if($receptorPunto->delete())
        {
            return Redirect::to('receptorespunto')->with(array('mensaje' => 'El receptor de punto de cuenta ha sido eliminado correctamente.'));
        }
        else 
        {
            return Redirect::to('receptorespunto')->withErrors('Ocurrio un error al intentar eliminar el receptor de punto de cuenta.');
        }
    }

    private function validate($inputs = array(), $registro = false)
    {

        $reglas = array(
            'nombre'         => array('required','min:3','max:32'),
        );

        if($registro) {
            $reglas['nombre'][] = 'unique:receptorespunto,nombre';
        }else {
            $reglas['nombre'][] = 'unique:receptorespunto,nombre,'.$inputs['id'];
        }
        
        return Validator::make($inputs, $reglas);
    }
}
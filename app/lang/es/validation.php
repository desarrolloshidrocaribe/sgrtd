<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "The :attribute must be accepted.",
	"active_url"           => "The :attribute is not a valid URL.",
	"after"                => "The :attribute must be a date after :date.",
	"alpha"                => "El campo :attribute solo puede contener letras.",
	"alpha_dash"           => "The :attribute may only contain letters, numbers, and dashes.",
	"alpha_num"            => "The :attribute may only contain letters and numbers.",
	"array"                => "The :attribute must be an array.",
	"before"               => "The :attribute must be a date before :date.",
	"between"              => array(
		"numeric" => "The :attribute must be between :min and :max.",
		"file"    => "The :attribute must be between :min and :max kilobytes.",
		"string"  => "The :attribute must be between :min and :max characters.",
		"array"   => "The :attribute must have between :min and :max items.",
	),
	"boolean"              => "The :attribute field must be true or false",
	"confirmed"            => "El campo :attribute no coincide con su confirmación.",
	"date"                 => "El campo :attribute no contiene una fecha valida.",
	"date_format"          => "The :attribute does not match the format :format.",
	"different"            => "The :attribute and :other must be different.",
	"digits"               => "The :attribute must be :digits digits.",
	"digits_between"       => "The :attribute must be between :min and :max digits.",
	"email"                => "El  campo :attribute debe contener un correo electrónico válido.",
	"exists"               => "The selected :attribute is invalid.",
	"image"                => "The :attribute must be an image.",
	"in"                   => "The selected :attribute is invalid.",
	"integer"              => "The :attribute must be an integer.",
	"ip"                   => "The :attribute must be a valid IP address.",
	"max"                  => array(
		"numeric" => "El campo :attribute no puede tener mas de :max digitos.",
		"file"    => "El campo :attribute no puede tener mas de :max kilobytes.",
		"string"  => "El campo :attribute no puede tener mas de :max caracteres.",
		"array"   => "El campo :attribute no puede tener mas de :max elementos.",
	),
	"mimes"                => "The :attribute must be a file of type: :values.",
	"min"                  => array(
		"numeric" => "El campo :attribute no puede tener menos de :min digitos.",
		"file"    => "El campo :attribute no puede tener menos de :min kilobytes.",
		"string"  => "El campo :attribute no puede tener menos de :min caracteres.",
		"array"   => "El campo :attribute no puede tener menos de :min elementos.",
	),
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => "El campo :attribute es obligatorio.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "El campo :attribute es obligatorio cuando el campo :values no está vacío.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "El campo :attribute es obligatorio cuando el campo :values está vacío.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "The :attribute and :other must match.",
	"size"                 => array(
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	),
	"unique"               => "El :attribute indicado ya esta siendo utilizado.",
	"url"                  => "The :attribute format is invalid.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
		'usuario' => 'nombre de usuario',
		'password' => 'contraseña',
		'password_confirmation' => 'confirmar contraseña',
		'nivelacceso' => 'nivel de acceso',
		'telefonomovil' => 'teléfono móvil',
		'email' => 'correo electrónico',
		'receptoresrtd' => 'receptores de RTD',
		'otroreceptorrtd' => 'remitir a otro',
		'numerortd' => 'N°',
		'fechatramitacion' => 'fecha de tramitación',
		'fecharecepcion' => 'fecha de recepción',
		'numerodocumento' =>'N° de documento',
		'tipodocumento' => 'tipo de documento',
		'otrotipodocumento' => 'otro tipo de documento'),

);